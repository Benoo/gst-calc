﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GST_Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Amount ($):");
            var amountStr = Console.ReadLine();
            decimal amount;
            if(decimal.TryParse(amountStr, out amount))
            {
                var gstCalculator = new GstCalculator();
                Console.WriteLine("GST is ($): " + gstCalculator.Calc(amount).ToString("0.00"));
            }
            else
            {
                Console.WriteLine("The entered amount was not a number.");
            }
            Console.ReadLine();
        }
    }

    public class GstCalculator
    {
        public decimal Calc(decimal amount)
        {
            return amount - (amount / 1.1m);
        }
    }

}
